#!/usr/bin/python
import time
import datetime
import pifacedigitalio
from threading import Thread
import ephem
import math

def getdate():
    i = time.localtime(time.time())
    j = time.strftime('%Y-%m-%d-%H-%M-%S', i)
    k = datetime.datetime.now()
    return i, j, k

def get_solar_coordinates(t):
    me = ephem.Observer()
    me.date = ephem.Date(t)
    me.lat, me.lon = lat, lon
    s = ephem.Sun()
    s.compute(me)
    alt_radians = float(repr(s.alt))
    az_radians = float(repr(s.az))
    alt_degrees = round(alt_radians * 180 / math.pi, 2)
    az_degrees = round(az_radians * 180 / math.pi, 2)
    return az_degrees, alt_degrees

def flashing_light(id,t1,t2):
    while not stopped:
        pfd.leds[id].toggle()
        time.sleep(t1)
        pfd.leds[id].toggle()        
        time.sleep(t2)

def write_log(filename, content):
    f = open(filename, 'a')
    f.write(content + '\n')
    f.close()

def sprinkle(seconds):
    pfd.relays[0].value=1
    time.sleep(seconds)
    pfd.relays[0].value=0

def count_events(l,n,t):
    if n > len(l) -1: ## -1 because event[0] is the count of deletions
        return False
    else:
        t_latest = l[len(l) -1] ## latest event is last item on the list
        t_n_ago = l[len(l) -n] 
        return ((t_latest - t_n_ago) < t)



def process_motion(events, n1, t1, n2, t2):
    k = getdate()[2] ## this works, but k = datetime.datetime.now() doesn't...?
    now = time.time()
    datetime = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime(now))
    events.append(now)

    if len(events) < 3: ## len = 3 means there have been 2 events, which is the min required to calculate a difference
        t_delta = 0
    else:
        t_delta = round(now - events[len(events)-2],1)

    if len(events) > 200:  ## have to do this to avoid assignment to a variable from the parent function
        del events[1:-99]
        deletions = events.pop(0) + 100
        events.insert(0,deletions)

    deletions = events[0]
    event_id = len(events) -1 + deletions  ## -1 because element 0 is count of deletions
    a = count_events(events, n1, t1) ## true if n1 events in last t1 seconds
    b = count_events(events, n2, t2) ## true if n2 events in last t2 seconds
    az, alt  = get_solar_coordinates(k)[0],get_solar_coordinates(k)[1] 
    content = "{0}, event {1}, delta={2}, a={3}, b={4}, azimuth = {5}, altitude = {6}".format(datetime, str(event_id), str(t_delta), str(a), str(b), str(az), str(alt))
    if a and (not b):
        content = content + ", yes"
        print(content)
        sprinkle(15)
    else:
        content = content + ", no"
        print(content)
        time.sleep(3) ## allow the sensor to settle before returning to the main loop

    write_log(logfile, content)

    return events

def main():
    global pfd
    global stopped
    global lat
    global lon
    global events
    global logfile
    n1, t1, n2, t2 = 2, 12, 10, 300
    lat, lon = '51.45', '2.5833'
    stopped = False
    pfd=pifacedigitalio.PiFaceDigital()
    events = [0] ## first element will be used to count deleted events
    logfile = '/home/pi/cats/log/log-poll.txt'
    try:
        flash = Thread(target = flashing_light, args=(7,0.5,1,))
        flash.start()
        print "running catfighter, firing on {0} events in {1}s, but not if also {2} events in {3}s".format(str(n1), str(t1), str(n2), str(t2))
        while True:
            if  pfd.input_pins[7].value == 0:  ## 0 because the PIR seems to constantly send back 1, except when it detects motion
                events = process_motion(events, n1, t1, n2, t2)

            ## use the other buttons to manually fire the sprinkler for different durations
            if pfd.input_pins[3].value == 1:  ## half an hour
                sprinkle(1800)
            if pfd.input_pins[2].value == 1:  ## 5 mins
                sprinkle(300) 
            if pfd.input_pins[1].value == 1:  ## 30s
                sprinkle(30)
            if pfd.input_pins[0].value == 1:  ## 1s
                sprinkle(5)
            time.sleep(1) ## check the inputs once per second - settle time is 3 seconds so this is plenty of precision. process_motion waits 3s after a single positive, and sprinkler adds a 15s delay when triggered.         
    finally:
        stopped = True
        print("stopping")
        pfd.relays[0].value=0
        for led in [1,2,3,4,5,6,7]:
            pfd.leds[led].turn_off()

if __name__ == '__main__':
    main()

