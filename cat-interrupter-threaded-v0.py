#!/usr/bin/python
import time
import datetime
import pifacedigitalio
from threading import Thread
import ephem
import math

def getdate():
    i = time.localtime(time.time())
    j = time.strftime('%Y-%m-%d-%H-%M-%S', i)
    k = datetime.datetime.now()
    return i, j, k

def get_solar_coordinates(t):
    me = ephem.Observer()
    me.date = ephem.Date(t)
    me.lat, me.lon = lat, lon
    s = ephem.Sun()
    s.compute(me)
    alt_radians = float(repr(s.alt))
    az_radians = float(repr(s.az))
    alt_degrees = round(alt_radians * 180 / math.pi, 2)
    az_degrees = round(az_radians * 180 / math.pi, 2)
    return az_degrees, alt_degrees

def flashing_light(id,t1,t2):
    while not stopped:
        pfd.leds[id].toggle()
        time.sleep(t1)
        pfd.leds[id].toggle()        
        time.sleep(t2)

def write_log(filename, content):
    f = open(filename, 'a')
    f.write(content + '\n')
    f.close()

def sprinkle(seconds):
    pfd.relays[0].value=1
    time.sleep(seconds)
    pfd.relays[0].value=0

def count_events(l,n,t):
    if n > len(l) -1: ## -1 because event[0] is the count of deletions
        return False
    else:
        t_latest = l[len(l) -1] ## latest event is last item on the list
        t_n_ago = l[len(l) -n] 
        return ((t_latest - t_n_ago) < t)

def make_listener():
    events = [0] ## first element will be used to count deleted events
    logfile = '/home/pi/cats/log/log.txt'

    def motion(event):
        k = getdate()[2] ## this works, but k = datetime.datetime.now() doesn't...?
        now = time.time()
        datetime = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime(now))
        events.append(now)
        
        if len(events) < 3: ## len = 3 means there have been 2 events, which is the min required to calculate a difference
            t_delta = 0
        else:
            t_delta = round(now - events[len(events)-2],1)
        
        if len(events) > 200:  ## have to do this to avoid assignment to a variable from the parent function
            del events[1:-99]
            deletions = events.pop(0) + 100
            events.insert(0,deletions)
        
        deletions = events[0]
        event_id = len(events) -1 + deletions  ## -1 because element 0 is count of deletions
        a = count_events(events,2,10) ## true if 2 events in 10 seconds
        b = count_events(events,10,600) ## true if 10 events in last 10 minutes
        az, alt  = get_solar_coordinates(k)[0],get_solar_coordinates(k)[1] 
        content = "{0}, event {1}, delta={2}, a={3}, b={4}, azimuth = {5}, altitude = {6}".format(datetime, str(event_id), str(t_delta), str(a), str(b), str(az), str(alt))
        if a and (not b):
            sprinkle(15)
            content = content + ", yes"
        else:
            content = content + ", no"
        
        print(content)
        write_log(logfile, content)

    return motion

def main():
    global pfd
    global stopped
    global lat
    global lon
    lat, lon = '51.45', '2.5833'
    stopped = False
    pfd=pifacedigitalio.PiFaceDigital()
    listener = pifacedigitalio.InputEventListener(chip=pfd)
    listener.register(0,pifacedigitalio.IODIR_ON,make_listener(),settle_time=3)
    try:
        listener.activate()
        flash = Thread(target = flashing_light, args=(7,0.5,1,))
        flash.start()
        print "running cat fightin' man..."
        while True:
            time.sleep(1)                
    finally:
        stopped = True
        listener.deactivate()
        pfd.relays[0].value=0
        for led in [1,2,3,4,5,6,7]:
            pfd.leds[led].turn_off()

if __name__ == '__main__':
    main()

